#![no_std]

//use cortex_m::asm;
//use cortex_m_rt::entry;
use cortex_m_semihosting::{hprintln, hprint};


pub struct Gimli {
    state: [u32;12]
}

impl Gimli {

    pub fn new() -> Result<Gimli, &'static str> {
        //let mut state: [[u32;4];3] = [[0;4];3];
        let state: [u32;12] = [0;12];

        Ok(Gimli { state })
    }

//    pub fn update(&mut self, data: &[u8]) {
//        self.absorb(data);
//    }

    fn absorb_block(&mut self, data: &[u8]) {

        //hprintln!("Absorbing block: ");
        //for i in data.iter() {
        //    hprint!("{:x?}, ", i);
        //}
        //hprintln!();
        //hprintln!("Into state: ");
        //for i in self.state.iter() {
        //    hprint!("{:x?}, ", i);
        //}
        //hprintln!();

        self.state[0] ^= (data[0] as u32) | ((data[1] as u32) << 8) | ((data[2] as u32) << 16) | ((data[3] as u32) << 24);// as u32;
        self.state[1] ^= (data[4] as u32) | ((data[5] as u32) << 8) | ((data[6] as u32) << 16) | ((data[7] as u32) << 24);// as u32;
        self.state[2] ^= (data[8] as u32) | ((data[9] as u32) << 8) | ((data[10] as u32) << 16) | ((data[11] as u32) << 24);// as u32;
        self.state[3] ^= (data[12] as u32) | ((data[13] as u32) << 8) | ((data[14] as u32) << 16) | ((data[15] as u32) << 24);// as u32;

        //for i in 0..4 {
        //    self.state[i*4] ^= (data[4*i] as u32) | ((data[(4*i)+1] as u32) << 8) | ((data[(4*i)+2] as u32) << 16) | ((data[(4*i)+3] as u32) << 24);// as u32;
        //}
        //%hprintln!("New state before GIMLI Perm: ");
        //%for i in self.state.iter() {
        //%    hprint!("{:x?}, ", i);
        //%}
        //%hprintln!();

        self.gimli();
        //%hprintln!("New state after GIMLI Perm: ");
        //%for i in self.state.iter() {
        //%    hprint!("{:x?}, ", i);
        //%}
        //%hprintln!();
    }

    fn squeeze(&self) -> [u8;16] {
        let mut out: [u8;16] = [0;16];
       // hprintln!("Squeeszing state: ");
       // for i in self.state.iter() {
       //     hprint!("{:x?}, ", i);
       // }
       // hprintln!();
        out[0] = self.state[0].to_le_bytes()[0];
        out[1] = self.state[0].to_le_bytes()[1];
        out[2] = self.state[0].to_le_bytes()[2];
        out[3] = self.state[0].to_le_bytes()[3];
        /////
        out[4] = self.state[1].to_le_bytes()[0];
        out[5] = self.state[1].to_le_bytes()[1];
        out[6] = self.state[1].to_le_bytes()[2];
        out[7] = self.state[1].to_le_bytes()[3];
        /////
        out[8] = self.state[2].to_le_bytes()[0];
        out[9] = self.state[2].to_le_bytes()[1];
        out[10] = self.state[2].to_le_bytes()[2];
        out[11] = self.state[2].to_le_bytes()[3];
        /////
        out[12] = self.state[3].to_le_bytes()[0];
        out[13] = self.state[3].to_le_bytes()[1];
        out[14] = self.state[3].to_le_bytes()[2];
        out[15] = self.state[3].to_le_bytes()[3];
       // hprintln!("Squoze block: {:x?}", (self.state[0] as u32));
       // for i in out.iter() {
       //     hprint!("{:x?}, ", i);
       // }
       // hprintln!();
        out
    }

    fn gimli(&mut self){ 
        //hprintln!("GIMLI Perm");
        //hprintln!("Final State: ");
        //for i in self.state.iter() {
        //    hprintln!("{:x?}", i);
        //}
        let mut x;
        let mut y;
        let mut z;
        let mut aux: [u32;4];
        for r in (1..25).rev() {
            for j in 0..4 {
                x = self.state[j].rotate_left(24);
                y = self.state[j+4].rotate_left(9);
                z = self.state[j+8];
                ////
                self.state[j+8] = x ^ (z << 1) ^ ((y & z) << 2);
                self.state[j+4] = y ^ x ^ ((x | z) << 1);
                self.state[j] = z ^ y ^ ((x & y) << 3);
                //hprintln!("State after non-linear: {:?}", self.state);
            }
            if r%4 == 0 {
                aux = [self.state[1],self.state[0],self.state[3],self.state[2]];
                self.state[0] = aux[0];
                self.state[1] = aux[1]; 
                self.state[2] = aux[2]; 
                self.state[3] = aux[3]; 
                //hprintln!("State after small swap: {:?}", self.state);

                self.state[0] ^= 0x9e377900 ^ r;
                //hprintln!("State after const: {:?}", self.state);

            } else if r%4 == 2 {
                aux = [self.state[2],self.state[3],self.state[0],self.state[1]];
                self.state[0] = aux[0];
                self.state[1] = aux[1]; 
                self.state[2] = aux[2]; 
                self.state[3] = aux[3]; 
                //hprintln!("State after big swap: {:?}", self.state);

            } 
        }
        //hprintln!("Final State: ");
        //for i in self.state.iter() {
        //    hprintln!("{:x?}", i);
        //}
    }

    pub fn finalize(&mut self, data: &[u8]) -> [u8;32] {
        let rate = 16;
        let mut out: [u8;32] = [0;32];
        
        let len = data.len();
        let blocks = len/rate;
        let bytes_left = len%rate;
       
        //hprintln!("Data: {:x?}", data).unwrap();
        //hprintln!("Data size: {:?}", data.len()).unwrap();
        //hprintln!("Blocks: {:?}", blocks).unwrap();
        //hprintln!("Bytes left: {:?}", bytes_left).unwrap();
       
       for i in 0..blocks {
            self.absorb_block(&data[16*i..(16*(i+1))]);
        }

        let mut last_block: [u8;16] = [0;16];

        let mut start = len - bytes_left;
        for i in 0..bytes_left {
            last_block[i] = data[start];
            start += 1;
        }
        last_block[bytes_left] = 0x1;

        self.state[11] ^= 0x01000000;
        self.absorb_block(&last_block);
        //self.gimli();

        let h1 = self.squeeze();
        self.gimli();
        let h2 = self.squeeze();
        
        for (to, from) in out.iter_mut().zip(h1.iter().chain(h2.iter())) { *to = *from };

        
        //hprintln!("H1: {:?}",h1.len()).unwrap();
        //hprintln!("H2: {:?}",h2.len()).unwrap();
        //hprintln!("H: {:?}",out).unwrap();

        out
    }
    
    pub fn chain_digest(&mut self, data: &[u8], it: usize) -> [u8;32] {
        let mut out: [u8;32] = [0;32];
        out = self.finalize(&data);
        self.reset();

        for i in 1..it {
            out = self.finalize(&out);
            self.reset();
        }
        out
    }

    pub fn reset(&mut self) {
        self.state = [0;12];
    }
}
