#![no_std]
#![no_main]

// pick a panicking behavior
extern crate panic_halt; // you can put a breakpoint on `rust_begin_unwind` to catch panics
// extern crate panic_abort; // requires nightly
// extern crate panic_itm; // logs messages over ITM; requires ITM support
// extern crate panic_semihosting; // logs messages to the host stderr; requires a debugger

//use cortex_m::asm;
use cortex_m_rt::entry;
use cortex_m_semihosting::{debug, hprintln, hprint};

use ewots::{Wots};

use gimli::Gimli;

//use sha2::{Sha256, Sha512};

#[entry]
fn main() -> ! {
//    asm::nop(); // To not have main optimize to abort in release mode, remove when you add code

//    test_wots();
    test_gimli();
    test_gimli_none();
    test_wots();
    debug::exit(debug::EXIT_SUCCESS);

    loop {
        // your code goes here
    }
}

#[allow(dead_code)]
fn test_wots() {
    hprintln!("--- WOTS ---").unwrap();

    let data = "I have grown to love secrecy. \
                It seems to be the one thing that can make modern life mysterious or marvelous to us. \
                The commonest thing is delightful if only one hides it.".as_bytes();

    let mut seed = 1337;

    let wots = Wots::new(&mut seed).unwrap();
    let sign = wots.sign(&data).unwrap();
    let verify = sign.verify(&data);

    hprintln!("Signature: {:?}", verify).unwrap();

}

fn test_gimli() {
    hprintln!("--- GIMLI ---").unwrap();

    let data_raw = "There's plenty for the both of us, may the best Dwarf win.";
    let data = data_raw.as_bytes();
    let expected = [0xf4, 0x56, 0xf2, 0xda, 0x6b, 0xf5, 0xbb, 0xde, 0xd9, 0xb6, 0x3, 0x3e, 0x63, 0xf7, 0x56, 0x45, 0x37, 0xee, 0xc5, 0x41, 0xc7, 0x8f, 0x3c, 0x47, 0x1e, 0xca, 0x11, 0xbf, 0xfa, 0xe9, 0xf, 0x55];
    let mut gimli = Gimli::new().unwrap();
    let mut hash = gimli.finalize(&data);
    let mut assert = true;
    hprintln!("Data: {}", data_raw);
    hprintln!("Expected - Obtained");
    for (e,o) in expected.iter().zip(hash.iter_mut()) {
        assert ^= e == o;
        hprintln!("0x{:02x?} - 0x{:02x?}", e, o).unwrap();
    }
    hprintln!("Match: {}", assert);

}

fn test_gimli_none() {
    hprintln!("--- GIMLI ---").unwrap();

    let data_raw = "";
    let data = data_raw.as_bytes();
    let expected = [0x27, 0xae, 0x20, 0xe9, 0x5f, 0xbc, 0x2b, 0xf0, 0x1e, 0x97, 0x2b, 0x0, 0x15, 0xee, 0xa4, 0x31, 0xc2, 0xf, 0xc8, 0x81, 0x8f, 0x25, 0xbc, 0x6d, 0xbe, 0x66, 0x23, 0x22, 0x30, 0xdb, 0x35, 0x2f];

    let mut assert = true;
    let mut gimli = Gimli::new().unwrap();
    let mut hash = gimli.finalize(&data);

    hprintln!("Data: {}", data_raw);
    hprintln!("Expected - Obtained");
    for (e,o) in expected.iter().zip(hash.iter_mut()) {
        assert ^= e == o;
        hprintln!("0x{:02x?} - 0x{:02x?}", e, o).unwrap();
    }
    hprintln!("Match: {}", assert);

}
