#![no_std]

//crypto;
extern crate oorandom;
use oorandom::Rand64;

//extern crate wyhash;
//use wyhash::wyhash;
//use wyhash::wyrng;

use sha2::{Sha256, Digest};

use gimli::Gimli;

extern crate core;
//use core::default::Default;
use cortex_m_semihosting::{hprintln, hprint};
//use cortex_m::peripheral::{DWT, Peripherals}; //It seems QEMU cant handle counting cycles that well

// WOTS Constants
mod wots_const;

// SiTRUCTS
#[allow(dead_code)]
pub struct Wots<'a> {
    w: usize,
    s_key: SecretKey<'a>,
}

//#[derive(Default)]
pub struct WotsSign {
    sign: [[u8;wots_const::N];wots_const::L],
    p_key: [u8;wots_const::N],
}

#[allow(dead_code)]
struct SecretKey<'a> {
    seed: &'a mut u64,
    s_key: [[u8;wots_const::N];wots_const::L],
}

// IMPLEMENTATIONS
impl WotsSign {
    pub fn verify(&self, data: &[u8]) -> bool {
        if wots_const::DEBUG {
            hprintln!("\n#=#=#=# Verify #=#=#=#").unwrap();
        }
        let mut concat: [u8;wots_const::L] = [0;wots_const::L];
        let digest = digest(&data);

        if wots_const::DEBUG {
            hprint!("Data hash: ").unwrap();
            for i in &digest {
                hprint!("{:x?}", i).unwrap();
            }
            hprintln!().unwrap();
        }

        let base16 = to_basew(&digest);
        let cs = checksum(&base16);
        
        for (i,v) in base16.iter().chain(&cs).enumerate() {
            concat[i] = *v;
        }
        let mut verify: [[u8;32];wots_const::L] = [[0;32];wots_const::L];
        
        for (i,it) in concat.iter().enumerate() {
            if *it == 15 {
                verify[i] = self.sign[i];
            } else {
                verify[i] = chain_digest(&self.sign[i], (wots_const::W-*it as usize-1 as usize) as usize);
            }
        }

        let pk = digest_pkey(&verify);
        if wots_const::DEBUG {
            hprint!("Restored Public Key Y: ").unwrap();
            for i in &pk {
                hprint!("{:x?}", i).unwrap();
            }
            hprintln!().unwrap();
        }
        if wots_const::DEBUG {
            hprintln!("#=#=#=# Done #=#=#=#").unwrap();
        }

        self.p_key.iter().eq(pk.iter())

    }
}

impl Wots<'_> {
    pub fn new<'a>(seed: &'a mut u64) -> Result<Wots<'a>, &'static str> {
        if wots_const::DEBUG {
            hprintln!("\n+++++++++++++++++++++++++++++++++++++++++++++").unwrap();
            hprintln!("Debugging WOTS with the following parameters:").unwrap();
            hprintln!("Hash: {:?}", wots_const::HASH).unwrap();
            hprintln!("Security: {:?} bytes", wots_const::N).unwrap();
            hprintln!("Winternitz parameter: {:?}",wots_const::W).unwrap();
            hprintln!("L1: {:?}", wots_const::L1).unwrap();
            hprintln!("L2: {:?}", wots_const::L2).unwrap();
            hprintln!("L: {:?}", wots_const::L).unwrap();
            hprintln!("+++++++++++++++++++++++++++++++++++++++++++++").unwrap();
        }
        
        if wots_const::DEBUG_SKEY {
            hprintln!("\n#=#=#=# Secret Key #=#=#=#").unwrap();
        }
        let s_key = SecretKey::new(seed).unwrap();
        
        if wots_const::DEBUG_SKEY {
            hprintln!("#=#=#=# Done #=#=#=#").unwrap();
        }
        Ok( Wots{ w:wots_const::W, s_key } )
    }

    pub fn sign(&self, data: &[u8]) -> Result<WotsSign, &'static str> {
        if wots_const::DEBUG {
            hprintln!("\n#=#=#=# Sign #=#=#=#").unwrap();
        }
        let mut concat: [u8;wots_const::L] = [0;wots_const::L];

        let digest = digest(&data);
        
        if wots_const::DEBUG {
            hprint!("Data hash: ").unwrap();
            for i in &digest {
                hprint!("{:x?}", i).unwrap();
            }
            hprintln!().unwrap();
        }
        
        let p_key = self.gen_pkey();
        
        if wots_const::DEBUG {
            hprint!("Public Key Y: ").unwrap();
            for i in &p_key {
                hprint!("{:x?}", i).unwrap();
            }
            hprintln!().unwrap();
        }
       
       let mut sign: [[u8;32];wots_const::L] = [[0;32];wots_const::L];

        let base16 = to_basew(&digest);
        
        if wots_const::DEBUG {
            hprint!("Base16: ").unwrap();
            for i in &base16 {
                hprint!("{:x?}", i).unwrap();
            }
            hprintln!().unwrap();
        }


        let cs = checksum(&base16);
        
        if wots_const::DEBUG {
            hprint!("Checksum: ").unwrap();
            for i in &cs {
                hprint!("{:x?}", i).unwrap();
            }
            hprintln!().unwrap();
        }
        
        for (i,v) in base16.iter().chain(&cs).enumerate() {
            concat[i] = *v;
        }
        if wots_const::DEBUG {
            hprint!("concat: ").unwrap();
            for i in &concat {
                hprint!("{:x?}", i).unwrap();
            }
            hprintln!().unwrap();
        }

        for (i,it) in concat.iter().enumerate() {
            if *it > 0 {
                sign[i] = chain_digest(&self.s_key.s_key[i], *it as usize);
            } else {
                sign[i] = self.s_key.s_key[i];
            }
        }
        if wots_const::DEBUG {
            hprintln!("=+=+= Done =+=+=").unwrap();
        }
        Ok( WotsSign { sign, p_key } )
    }
    

    fn gen_pkey(&self) -> [u8;wots_const::N] {
        let mut out: [[u8;32];wots_const::L] = [[0;32];wots_const::L];
        for i in 0..wots_const::L {
            out[i] = chain_digest(&self.s_key.s_key[i], wots_const::W - 1);
        }
        let pk = digest_pkey(&out);
        pk
    }
}

impl SecretKey<'_> {
    fn new(seed: &mut u64) -> Result<SecretKey, &'static str> {
        let mut out: [[u8;wots_const::N];wots_const::L] = [[0;wots_const::N];wots_const::L];
        let mut rng = Rand64::new(*seed as u128); // TODO get a better PRNG
        let mut hasher = Sha256::new();
        let mut aux;
        for i in 0..wots_const::L {
            aux = rng.rand_u64();
            hasher.update(aux.to_ne_bytes());
            out[i] = hasher.finalize_reset().into();
        }
        if wots_const::DEBUG_SKEY {
            for (i, key) in out.iter().enumerate() {
                hprint!("s_key {:?}: ", i).unwrap();
                for byte in key {
                    hprint!("{:x?}", byte).unwrap();
                }
                hprintln!().unwrap();
            }
        }
        //hprintln!("S_KEY: {:?}", out);
        Ok( SecretKey{ seed, s_key:out } )
    }
}


// HASH
// GIMLI
#[cfg(feature = "GIMLI")]
fn chain_digest(data: &[u8], it: usize) -> [u8; wots_const::N] {
    let mut hasher = Gimli::new().unwrap();
    let digest: [u8; wots_const::N] = hasher.chain_digest(data, it).into();
    digest
}

#[cfg(feature = "GIMLI")]
fn digest(data: &[u8]) -> [u8; wots_const::N] {
    let mut hasher = Gimli::new().unwrap();
    let digest: [u8; wots_const::N] = hasher.finalize(data).into();
    //let mut digest: [u8; wots_const::N] = hasher.finalize().into();
    digest
}

#[cfg(feature = "GIMLI")]
fn digest_pkey(data: &[[u8;wots_const::N];wots_const::L]) -> [u8; wots_const::N] {
    let mut hasher = Gimli::new().unwrap();
    let mut concat: [u8;wots_const::N*wots_const::L] = [0;wots_const::N*wots_const::L];
    
    let mut i = 0;
    for pk in data {
        for bt in pk {
            concat[i] = *bt;
            i += 1;
        }
    }

    let digest: [u8; wots_const::N] = hasher.finalize(&concat).into();
    digest
}

// SHA256
#[cfg(feature = "SHA256")]
fn chain_digest(data: &[u8], it: usize) -> [u8; wots_const::N] {
    let mut hasher = Sha256::new();
    hasher.update(&data);
    let mut digest: [u8; wots_const::N] = hasher.finalize_reset().into();
    for _ in 1..it {
        hasher.update(&digest);
        digest = hasher.finalize_reset().into();
    }
    digest
}

#[cfg(feature = "SHA256")]
fn digest(data: &[u8]) -> [u8; wots_const::N] {
    let mut hasher = Sha256::new();
    hasher.update(&data);
    let digest: [u8; wots_const::N] = hasher.finalize().into();
    //let mut digest: [u8; wots_const::N] = hasher.finalize().into();
    digest
}

#[cfg(feature = "SHA256")]
fn digest_pkey(data: &[[u8;wots_const::N];wots_const::L]) -> [u8; wots_const::N] {
    let mut hasher = Sha256::new();
    for d in data {
        hasher.update(&d);
    }
    let digest: [u8; wots_const::N] = hasher.finalize().into();
    //let mut digest: [u8; wots_const::N] = hasher.finalize().into();
    digest
}

// Kecchak

// WOTS16
#[cfg(feature = "16")]
fn to_basew(data: &[u8]) -> [u8;wots_const::L1] {
    let mut out: [u8;wots_const::L1] = [0;wots_const::L1];

    let mut it = 0;
    for i in data.iter() {
        out[it] = i >> wots_const::LOG2W;
        out[it+1] = (i << wots_const::LOG2W) >> wots_const::LOG2W;

        it += 2;
    }
    out
}

#[cfg(feature = "16")]
fn checksum(data: &[u8;wots_const::L1]) -> [u8;wots_const::L2] {
    let mut out: [u8;wots_const::L2] = [0;wots_const::L2];
    let mut aux: u16 = 0;

    for i in data {
        aux += wots_const::W as u16 - 1 - *i as u16;
    }
    let aux_bytes = aux.to_ne_bytes();
    out[0] = aux_bytes[0] >> wots_const::LOG2W;
    out[1] = (aux_bytes[0] << wots_const::LOG2W) >> wots_const::LOG2W;
    out[2] = aux_bytes[1];

    out
}

// WOTS256
#[cfg(feature = "256")]
fn to_basew(data: &[u8]) -> [u8;wots_const::L1] {
    let mut out: [u8;wots_const::L1] = [0;wots_const::L1];

    let mut it = 0;
    for i in data.iter() {
        out[it] = *i;
        it += 1;
    }
    out
}

#[cfg(feature = "256")]
fn checksum(data: &[u8;wots_const::L1]) -> [u8;wots_const::L2] {
    let mut out: [u8;wots_const::L2] = [0;wots_const::L2];
    let mut aux: usize = 0;

    for i in data {
        aux += *i as usize;
    }

    for (i,v) in aux.to_ne_bytes().iter().enumerate() {
        out[i] = *v;
    }
    out
}


//const N: usize = 64;
//pub struct MyRngSeed(pub [u8; N]);
//pub struct MyRng(MyRngSeed);
//
//impl Default for MyRngSeed {
//    fn default() -> MyRngSeed {
//        MyRngSeed([0; N])
//    }
//}
//
//impl AsMut<[u8]> for MyRngSeed {
//    fn as_mut(&mut self) -> &mut [u8] {
//        &mut self.0
//    }
//}
//
//impl SeedableRng for MyRng {
//    type Seed = MyRngSeed;
//
//    fn from_seed(seed: MyRngSeed) -> MyRng {
//        MyRng(seed)
//    }
//}
//
//










