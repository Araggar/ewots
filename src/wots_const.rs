// DEBUG
#[cfg(feature = "DEBUG")]
pub const DEBUG: bool = true;
#[cfg(feature = "NO_DEBUG")]
pub const DEBUG: bool = false;
#[cfg(feature = "DEBUG_SKEY")]
pub const DEBUG_SKEY: bool = true;
#[cfg(feature = "NO_DEBUG_SKEY")]
pub const DEBUG_SKEY: bool = false;


// HASH FUNCTION
// SHA-256
#[cfg(feature = "SHA256")]
pub const N: usize = 32;
#[cfg(feature = "SHA256")]
pub const HASH: &str = "SHA256";
// GIMLI
#[cfg(feature = "GIMLI")]
pub const N: usize = 32;
#[cfg(feature = "GIMLI")]
pub const HASH: &str = "GIMLI";

// WOTS 16
//#[cfg(feature = "16")]
//pub const M: usize = 256;
#[cfg(feature = "16")]
pub const W: usize = 16;
#[cfg(feature = "16")]
pub const LOG2W: usize = 4;
#[cfg(feature = "16")]
pub const L: usize = 67;
#[cfg(feature = "16")]
pub const L1: usize = 64;
#[cfg(feature = "16")]
pub const L2: usize = 3;

// WOTS 256
#[cfg(feature = "256")]
pub const W: usize = 16;
#[cfg(feature = "256")]
pub const LOG2W: usize = 8;
#[cfg(feature = "256")]
pub const L: usize = 10;
#[cfg(feature = "256")]
pub const L1: usize = 8;
#[cfg(feature = "256")]
pub const L2: usize = 2;

